Title: xorg-server unmask 1.15
Author: Paul Seidler <sepek@exherbo.org>
Content-Type: text/plain
Posted: 2014-03-28
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: x11-server/xorg-server[<1.15]

Xorg-server 1.15 isn't masked anymore. After installing it you need to reinstall all xf86-drivers for it.
